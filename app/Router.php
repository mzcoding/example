<?php namespace App;



 class Router 
 {
     private $controller, $action, $container, $params;
     /**
      *  Если в адресной строке параметр ?
      *  ?с = login - Это controller
      *  ?a - action - Это action
      */

     /**
      * Router constructor.
      * @param $container
      */
     public function __construct($container)
     {
        $request = $container['request'];

        $this->container = $container;
        $controller = $request->get('c') ?? 'Users';
        $this->controller = ucfirst($controller)."Controller";
        $this->action = $request->get('a') ?? 'index';
        $this->params = intval($request->get('id')) ?? 0;

     }

     /**
      * Подключаем классы динамически
      */
     public function init()
     {
        $className = 'App\\Controllers\\' . $this->controller;

        if(!class_exists($className)) {
            abort404();
        }
        $objController = new  $className($this->container);
        $this->params = ($this->params) ? $objController->{$this->action}($this->params)
                                        : $objController->{$this->action}();
     }
 }