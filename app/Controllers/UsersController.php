<?php


namespace App\Controllers;


use App\Models\User;


class UsersController extends BaseController
{
    /**
     * Возвращаем список клиентов
     */
   public function index()
   {
       $request = $this->di['request'];
       $objUser = new User($this->di['db']);
       $q = $request->get('q') ?? null;
       $prepare = ($q) ? $objUser->searchData($q) : $objUser->getData();


       $clients = array_map(function($data) {
           if(isset($data['phones']) && $data['phones']) {
               $data['phones'] = explode(',', $data['phones']);
           }
           return $data;
       }, $prepare);


       return $this->response([
           'success'    => true,
           'clients'    => $clients
       ]);
   }

    /**
     * @return string
     */
   public function addClient()
   {
       $request = $this->di['request'];

       if($request->isMethod('POST')) {
           $content = $request->getContent();
           $all = json_decode($content, true);


           $first_name = $all['first_name'] ?? null;
           $last_name  = $all['last_name'] ?? null;
           $country    = $all['country'] ?? null;
           //валидация может быть


           //************//

           $objUser = new User($this->di['db']);
           $status = $objUser->insertData([
               'first_name' => $first_name,
               'last_name'  => $last_name,
               'country'    => $country
           ]);
           if((bool)$status === true) {
               return $this->response(['success' => true]);
           }

           return $this->response(['success' => false, 'message' => 'Не удалось добавить в бд'], 400);

       }

       return $this->response(['success' => false, 'message' => 'Не POST запрос'], 400);

   }

    /**
     * @return string
     */
   public function editClient()
   {
       $request = $this->di['request'];
       $id = (int)$request->get('id');
       $objUser = new User($this->di['db']);
       $result = $objUser->getItem($id);

       if($result) {
           $params = [
               'success' => true,
               'id' => $id,
               'first_name' => $result->first_name,
               'last_name' => $result->last_name,
               'country' => $result->country
           ];

           return $this->response($params);
       }

       return $this->response(['success' => false, 'message' => 'Чтото пошло не так'], 400);

   }
   public function editRequestClient()
   {
       $request = $this->di['request'];

       if($request->isMethod('POST')) {
           $content = $request->getContent();
           $all = json_decode($content, true);
           $id = (int)$all['id'];

           $first_name = $all['first_name'] ?? null;
           $last_name  = $all['last_name'] ?? null;
           $country    = $all['country'] ?? null;

           //валидация может быть



           $objUser = new User($this->di['db']);
           $objUser->editClient($first_name, $last_name, $country, $id);
           return $this->response(['success' => true]);
       }

       return $this->response(['success' => false], 400);

   }
   public function deleteClient()
   {

       $request = $this->di['request'];
       $id = (int)$request->get('id');
       $objUser = new User($this->di['db']);
       $objUser->deleteClient($id);


       return $this->response(['success' => true]);
   }

  public function addPhone()
  {
      $request = $this->di['request'];
      if($request->isMethod('POST')) {
          $content = $request->getContent();
          $all = json_decode($content, true);
          $user_id = (int)$all['id'];
          $phone = $all['phone'] ?? null;

          $objUser = new User($this->di['db']);
          $objUser->addPhone([
              'user_id' => $user_id,
              'phone'   => $phone
          ]);

          return $this->response(['success' => true]);
      }
  }



}