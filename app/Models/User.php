<?php
namespace App\Models;


class User extends Model
{

   protected $table = "users";
   public function IsRefreshStatus($user): bool
   {
      $id = (int)$user->id;
      $status = $user->status;
      $sql = "SELECT status FROM users WHERE id = $id";
      $query = $this->db->query($sql);
      $r = $query->fetchObject();

       if((bool)$r->status !== $status) {
           $user->status = (bool)$r->status;
           return true;
       }

       return false;
   }


    /**
     * @param array $data
     * @return bool
     */
   public function addPhone(array $data)
   {
       $sqlPrepare = "INSERT INTO phones(`user_id`, `phone`)
                             VALUES('".$data['user_id']."','".$data['phone']."')";
       $query = $this->db->query($sqlPrepare);
       if($query) {
           return true;
       }
       return false;
   }
   public function getPhones(int $user_id)
   {
       $sqlPrepare = "SELECT * FROM phones WHERE user_id = $user_id";
       $query = $this->db->query($sqlPrepare);

       return $query->fetchAll(\PDO::FETCH_ASSOC);
   }

    /**
     * @param array $data
     * @return bool|object
     */
   public function addClient(array $data)
   {
       $status = $this->insertData($data);
       return $status;
   }

    /**
     * @param $first_name
     * @param $last_name
     * @param $country
     * @param $id
     * @return bool
     */
   public function editClient($first_name, $last_name, $country, $id)
   {
       $sqlPrepare = "UPDATE users SET first_name = :first_name, last_name = :last_name, country = :country
                      WHERE id = :id";
       $sth = $this->db->prepare($sqlPrepare);
       $sth->bindParam(":first_name", $first_name);
       $sth->bindParam(":last_name", $last_name);
       $sth->bindParam(":country", $country);
       $sth->bindParam(":id", $id);
       return $sth->execute();
   }
   public function deleteClient(int $id)
   {
        $sqlPrepare = "DELETE FROM phones WHERE user_id = $id";
        $sqlSecondPrepare =  "DELETE FROM users WHERE id = $id";

        $this->db->query($sqlPrepare);
        $this->db->query($sqlSecondPrepare);
   }




}