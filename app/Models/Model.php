<?php
namespace App\Models;


class Model
{
   protected $db, $table;
   public function __construct(\PDO $db)
   {
       $this->db = $db;
   }

    /**
     * Вывод любых данных
     * @return array
     */
    public function getData()
    {

        $query = $this->db->query("SELECT users.id as id, users.first_name as first_name, users.last_name as last_name, users.country as country, 
                                                   GROUP_CONCAT(phones.phone) as phones 
                                             FROM users LEFT JOIN phones 
                                             ON users.id = phones.user_id
                                             GROUP BY `id`");


        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
    public function getItem(int $id)
    {
        $query = $this->db->query("SELECT * from {$this->table} 
                                            WHERE id = $id");
        return $query->fetchObject();
    }

    /**
     *  Добавление любых данных
     * @param array $params
     * @return bool|object
     */
    public function insertData(array $params)
    {
        $values = [];
        $out = "";
        $i = 0;
        foreach($params as $key => $value) {
            $values[] = $value;
            $out .=  ($i) ? ",?" : "?"; $i++;
        }

        $sqlPrepare = "INSERT INTO users(".implode(",",
                array_keys($params)).")
                             VALUES($out)";

        $statement = $this->db->prepare($sqlPrepare);
        $status = $statement->execute($values);

        if($status) {
            $id = $this->db->lastInsertId('users_id_seq');
            return $this->getItem($id);
        }
        return $status;
    }
    public function searchData($q)
    {
        $query = $this->db->prepare("SELECT users.id as id, users.first_name as first_name, users.last_name as last_name, users.country as country, 
                                                   GROUP_CONCAT(phones.phone) as phones 
                                             FROM users  LEFT JOIN phones 
                                             ON users.id = phones.user_id
                                             WHERE first_name LIKE :q OR last_name LIKE :q
                                             GROUP BY `id`");
        $query->bindValue(':q', "%{$q}%");
        $query->execute();

        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
}