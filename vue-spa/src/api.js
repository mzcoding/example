import Vue from 'vue';
import axios from 'axios';
Vue.use(axios);



const __API_ROOT = 'https://sandbox.app/test';
const config = {
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    }
};
export default(
    {

        addPhone: (options) =>
            axios.post(`${__API_ROOT}/?c=users&a=addPhone`, options, config),
        getClients: () =>
            axios.get(`${__API_ROOT}`),
        searchClients: (q) =>
            axios.get(`${__API_ROOT}/?q=${q}`),
        getClientById: (id) =>
            axios.get(`${__API_ROOT}/?c=users&a=editClient&id=${id}`),

        deleteClient: (id) =>
            axios.get(`${__API_ROOT}/?c=users&a=deleteClient&id=${id}`),

        updateClient: (options) =>
            axios.post(`${__API_ROOT}?c=users&a=editRequestClient`, options, config),

        addClient: (options) =>
            axios.post(`${__API_ROOT}?c=users&a=addClient`, options, config),


    }
);