import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import Home from './Home.vue'
import Contact from './Contact.vue'


Vue.use(Router)
const router = new Router({
    routes: [
        {
            path: '/',
            name:'home',
            component: Home,
        },
        {
            path: '/client/add',
            name:'client_add',
            component: Contact,
            props: true,
        },
        {
            path: '/client/:id',
            name:'client_edit',
            component: Contact,
            props: true,
        }]
});

new Vue({
    el: '#app',
    render: h => h(App),
    router
})
