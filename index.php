<?php
//инициализируем сессию
session_start();

header("Access-Control-Allow-Origin: *");

//Подключаем зависимости

 require_once __DIR__ . '/vendor/autoload.php';
 //Подключаем конфигурацию

 $config = include (__DIR__ . '/config/config.php');
 define("ROOT_PATH", __DIR__);

 //Отлавливаем все эксеппшены, так-же можно логировать в будущем
try{
    //Initial request
    /**  Это симфони компонент отвечающий за работу всех глобальных переменных
     */
    $request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    //DI контейнер

    $container = new Pimple\Container();
    /** Добавляем конфиг в зависимости */
    $container['config'] = $config;
    //Request data
    $container['request'] = $request;
    //Sessions
    $container['session'] = $container->factory(function ($c) {
        $session = new \App\Classes\Session();
        return $session;
    });
    //БД
    $container['db'] = function () use(&$config) {
        $host = $config['db']['host'] ?? 'localhost';
        $database = $config['db']['database'] ?? 'postgres';
        $user = $config['db']['user'] ?? 'postgres';
        $password = $config['db']['password'] ?? '';

        $dsn = "mysql:host=$host;dbname=$database";

        $dbh = new \PDO($dsn,$user,$password);
        if($dbh){
            $log =  "Connected to the <strong>$database</strong> database successfully!";
        }
        return $dbh;
    };


    //Инициализируем вход

    (new App\Router($container))->init();


}catch(\App\Exceptions\LkException $e){
    echo $e->getMessage(); //Ошибки возникшие в коде текущего скрипта
}catch (\PDOException $e){
     echo $e->getMessage(); //ошибки БД
}catch(\Exception $e) {
    echo $e->getMessage(); //Прочие ошибки
}
 
